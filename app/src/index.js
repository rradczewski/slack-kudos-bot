#!/usr/bin/env node

const debug = require("debug");
debug.enable("kudos:*");

const getFromEnvOrDie = (name) => {
  if(process.env[name]) {
    return process.env[name];
  }
  console.error(`${name} is not set. Please edit /etc/default/slack-kudos`);
  process.exit(1);
}

const SLACK_TOKEN = getFromEnvOrDie("SLACK_TOKEN");
const PRINTER = getFromEnvOrDie("PRINTER");


const Bot = require("./bot");
const createSlack = require("./slack");
const Printer = require("./printer");

const run = async () => {
  const slack = await createSlack(SLACK_TOKEN);
  const printer = new Printer(PRINTER);
  const bot = new Bot({ slack, printer });
};

run();
