const cmd = require("./cmd");
const Printer = require("./printer");

jest.mock("./cmd");

describe("Printer", () => {
  let printer;
  beforeEach(() => {
    printer = new Printer("name-of-printer", { timeout: 100 });
    jest.resetAllMocks();

    cmd.get_async.mockImplementation(async cmd => {
      if (cmd.startsWith("lp "))
        return { stdout: "request id is cups-pdf-3 (1 file(s))", stderr: "" };

      if (cmd.startsWith("lpstat"))
        return {
          stdout:
            "cups-pdf-3              root             75776   Sat 20 Apr 2019 09:57:59 PM UTC",
          stderr: ""
        };
    });
  });

  it("should invoke lp to print a given file to the printer", async () => {
    await printer.print("file.pdf");
    expect(cmd.get_async).toHaveBeenCalledWith(
      `lp -d "name-of-printer" -o fit-to-page "file.pdf"`
    );
  });

  it("should resolve if the file was printed successfully", async () => {
    await printer.print("file.pdf");
    expect(cmd.get_async).toHaveBeenCalledWith(
      `lpstat -P "name-of-printer" -W completed`
    );
  });

  it("should fail if the file has not been printed after 2000 seconds", async () => {
    cmd.get_async.mockImplementation((cmd, cb) => {
      if (cmd.startsWith("lp "))
        return { stdout: "request id is cups-pdf-3 (1 file(s))" };

      if (cmd.startsWith("lpstat")) {
        return { stdout: "" };
      }
    });

    await expect(printer.print("file.pdf")).rejects.toThrow();
  });

  it("should retry if the file has not been printed yet", async () => {
    let invocations = 0;
    cmd.get_async.mockImplementation((cmd, cb) => {
      if (cmd.startsWith("lp "))
        return { stdout: "request id is cups-pdf-3 (1 file(s))" };

      if (cmd.startsWith("lpstat")) {
        if (invocations > 3) {
          return {
            stdout:
              "cups-pdf-3              root             75776   Sat 20 Apr 2019 09:57:59 PM UTC"
          };
        } else {
          invocations++;
          return { stdout: "" };
        }
      }
    });

    await printer.print("file.pdf");

    const numOfInvocations = cmd.get_async.mock.calls.length;
    expect(numOfInvocations).toBeGreaterThan(1);
  });
});
