app_version    := $(shell jq -r .version app/package.json)
date					 := $(shell date +"%F")

image: pi-gen/deploy/$(date)-kudos-raspbian-lite.img

.PHONY: clean
clean:
	-docker rm -f pigen_work
	-rm pi-gen/stage3-kudos/00-app-dependencies/files/slack-kudos_$(app_version)_all.deb

pi-gen/deploy/$(date)-kudos-raspbian-lite.img: pi-gen/stage3-kudos/00-app-dependencies/files/slack-kudos_$(app_version)_all.deb
	(cd pi-gen; CONTINUE=1 ./build-docker.sh)

pi-gen/stage3-kudos/00-app-dependencies/files/slack-kudos_$(app_version)_all.deb: app/slack-kudos_$(app_version)_all.deb
	cp app/slack-kudos_1.0.0_all.deb pi-gen/stage3-kudos/00-app-dependencies/files

app/slack-kudos_$(app_version)_all.deb: app/src/* app/templates/* app/*.json
	(cd app; docker build -f Dockerfile.mkdeb -t slack-kudos-mkdeb:latest .)
	(cd app; docker run --rm -v $(shell pwd)/app:/usr/src/app slack-kudos-mkdeb)
